import 'package:rebraingram/api.dart';
import 'package:rebraingram/routing/routes.dart';
import 'package:rebraingram/state/callback.dart';
import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/state/home.dart';
import 'package:rebraingram/state/initial.dart' show InitialState, CredentialsChanged;
import 'package:rebraingram/state/login.dart';
import 'package:rebraingram/state/new_post.dart';


class RegramController extends AppStateController {
  final RegramAPI _api;

  RegramController(this._api) : super(InitialState());


  AppState? _navigate(AppState? currentState, RegramRoute route) {
    if (route is CallbackRoute) {
      return CallbackState(route.code);
    }

    if (currentState == null) {
      return InitialState();
    }


    if (route is FeedRoute) {
      return FeedState();
    }

    if (route is NewPostRoute) {
      return NewPostState();
    }
    return ProfileState(null);
  }

  @override
  AppState? process(AppState currentState, AppStateEvent event) {
    if (currentState is CallbackState) {
      return null;
    }

    if (event is CredentialsChanged) {
      if (event.newCredentials == null) {
        return LoginState();
      }

      _api.credentials = event.newCredentials;

      return ProfileState(null);
    } else if (event is NavigateEvent) {
      return _navigate(currentState, event.route);
    }


    return null;
  }
}
