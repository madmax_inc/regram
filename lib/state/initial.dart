import 'package:flutter/material.dart';

import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/api.dart';
import 'package:rebraingram/routing/routes.dart';

class CredentialsChanged extends AppStateEvent {
  final APICredentials? newCredentials;

  const CredentialsChanged(this.newCredentials);
}

class InitialState extends AppState {
  RegramRoute asRoute() {
    return HomeRoute();
  }
}

class InitialStateView extends AppStateView<InitialState> {
  InitialStateView(AppStateEventListener listener) : super(listener);

  @override
  List<Page> build(InitialState state) {
    return [
      MaterialPage(
        key: ValueKey("HomePage"),
        child: FutureBuilder<void>(
          future: () async {
            listener.eventHappened(CredentialsChanged(await APICredentials.restore()));
          }(),
          builder: (context, snapshot) {
            return Center(child: Text("Please, wait while we're loading the initial state..."));
          },
        )
      )
    ];
  }
}
