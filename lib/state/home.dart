import 'package:flutter/material.dart';
import 'package:rebraingram/api.dart';
import 'package:rebraingram/routing/routes.dart';
import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/ui/home.dart';


abstract class HomeState extends AppState {
  const HomeState();
}


class ProfileState extends HomeState {
  final String? profileId;

  const ProfileState(this.profileId);

  @override
  RegramRoute asRoute() {
    if (profileId == null) {
      return HomeRoute();
    }

    return ProfileRoute(profileId!);
  }
}

class FeedState extends HomeState {
  @override
  RegramRoute asRoute() {
    return FeedRoute();
  }
}

class HomeView extends AppStateView<HomeState> {
  final RegramAPI _api;

  HomeView(AppStateEventListener listener, this._api) : super(listener);

  @override
  List<Page> build(HomeState state) {
    if (state is ProfileState) {
      if (state.profileId == null) {
        return [
          MaterialPage(key: ValueKey("Home Page"), child: HomeScreen((event) {listener.eventHappened(event);}, MyProfileSubState(_api)))
        ];
      }
    } else if (state is FeedState) {
      return [
        MaterialPage(key: ValueKey("Home Page"), child: HomeScreen((event) {listener.eventHappened(event);}, FeedSubState(_api)))
      ];
    }

    return [];
  }

}