import 'package:flutter/widgets.dart';
import 'package:rebraingram/routing/routes.dart';

abstract class AppStateEvent {
  const AppStateEvent();
}

class BackInvokedEvent extends AppStateEvent {}
class NavigateEvent extends AppStateEvent {
  final RegramRoute route;

  const NavigateEvent(this.route);
}

abstract class AppStateEventListener {
  void eventHappened(AppStateEvent event);
}

abstract class AppState {
  const AppState();

  RegramRoute asRoute();
}

abstract class AppStateController implements AppStateEventListener {
  AppState state;
  AppStateEventListener? delegate = null;

  AppStateController(this.state);

  AppState? process(AppState currentState, AppStateEvent event);

  @override
  void eventHappened(AppStateEvent event) {
    final newState = process(state, event);

    if (newState == null) {
      return;
    }
    print(event);
    print('new state ');
    print(newState);

    state = newState;
    delegate?.eventHappened(event);
  }
}

abstract class AppStateView<T extends AppState> {
  final AppStateEventListener listener;

  AppStateView(this.listener);

  List<Page> build(T state);
}
