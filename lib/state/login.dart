import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/navigator.dart';
import 'package:flutter/widgets.dart';

import 'package:rebraingram/routing/routes.dart';
import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/state/initial.dart';
import 'package:rebraingram/ui/login.dart';
import 'package:rebraingram/api.dart';

class LoginState extends AppState {
  @override
  RegramRoute asRoute() {
    return HomeRoute();
  }
}

class LoginView extends AppStateView<LoginState> {
  final RegramAPI api;
  LoginView(AppStateEventListener listener, this.api) : super(listener);

  @override
  List<Page> build(LoginState state) {
    return [
      MaterialPage(
        key: ValueKey("LoginPage"),
        child: LoginScreen(
          api,
          onLoggedIn: (credentials) {
            listener.eventHappened(CredentialsChanged(credentials));
          },
        )
      )
    ];
  }
}
