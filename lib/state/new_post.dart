import 'package:flutter/material.dart';
import 'package:rebraingram/api.dart';
import 'package:rebraingram/routing/routes.dart';
import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/ui/home.dart';
import 'package:rebraingram/ui/new_post.dart';


class NewPostState extends AppState {
  @override
  RegramRoute asRoute() {
    return NewPostRoute();
  }

}


class NewPostView extends AppStateView<NewPostState> {
  final RegramAPI _api;

  NewPostView(AppStateEventListener listener, this._api) : super(listener);

  @override
  List<Page> build(NewPostState state) {
      return [
        MaterialPage(key: ValueKey("New Post Page"), child: NewPostWidget(_api, listener.eventHappened)),
      ];
  }

}