import 'dart:html';

import 'package:flutter/material.dart';
import 'package:rebraingram/routing/routes.dart';

import 'common.dart';

class CallbackState extends AppState {
  final String code;

  CallbackState(this.code);

  @override
  RegramRoute asRoute() {
    return CallbackRoute(code);
  }
}

class CallbackView extends AppStateView<CallbackState> {
  CallbackView(AppStateEventListener listener) : super(listener);

  @override
  List<Page> build(CallbackState state) {
    window.opener!.postMessage(state.code, "*");
    return [MaterialPage(child: Text(""))];
  }
}

