import 'package:flutter/src/widgets/navigator.dart';
import 'package:rebraingram/state/callback.dart';
import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/state/home.dart';
import 'package:rebraingram/state/initial.dart';
import 'package:rebraingram/state/login.dart';
import 'package:rebraingram/state/new_post.dart';


class RegramView extends AppStateView {
  final InitialStateView initialStateView;
  final LoginView loginView;
  final CallbackView callbackView;
  final HomeView homeView;
  final NewPostView newPostView;

  RegramView(AppStateEventListener listener, this.initialStateView, this.loginView,
      this.callbackView, this.homeView, this.newPostView) : super(listener);

  @override
  List<Page> build(AppState state) {
    if (state is InitialState) {
      return initialStateView.build(state);
    } else if (state is LoginState) {
      return loginView.build(state);
    } else if (state is CallbackState) {
      return callbackView.build(state);
    } else if (state is HomeState) {
      return homeView.build(state);
    } else if (state is NewPostState) {
      return newPostView.build(state);
    }

    return [];
  }
}