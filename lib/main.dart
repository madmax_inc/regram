import 'package:flutter/material.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:rebraingram/state/new_post.dart';

import 'api.dart';

import 'routing/routes.dart';

import 'state/controller.dart';
import 'state/initial.dart';
import 'state/view.dart';
import 'state/callback.dart';
import 'state/home.dart';
import 'state/login.dart';

import 'ui/delegate.dart';

void main() {
  setUrlStrategy(PathUrlStrategy());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final RegramAPI _api = RegramAPI(APIConfig.devConfig);
  late final RegramController _controller;
  late final RegramView _view;
  late final RegramRouterDelegate _delegate;

  MyApp({Key? key}) : super(key: key) {
    _controller = RegramController(_api);
    _view = RegramView(_controller, InitialStateView(_controller), LoginView(_controller, _api),
                      CallbackView(_controller), HomeView(_controller, _api), NewPostView(_controller, _api));
    _delegate = RegramRouterDelegate(_controller, _view);
    _controller.delegate = _delegate;
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routerDelegate: _delegate,
      routeInformationParser: RegramRouteParser(),
    );
  }
}
