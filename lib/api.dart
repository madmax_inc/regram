import 'dart:convert';
import 'dart:typed_data';

import 'package:rebraingram/oauth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class APICredentials {
  final String accessToken;
  final String refreshToken;

  const APICredentials(this.accessToken, this.refreshToken);

  static Future<APICredentials?> restore() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final accessToken = prefs.getString("accessToken");
    final refreshToken = prefs.getString("refreshToken");

    if (accessToken == null || refreshToken == null) {
      return null;
    }

    return APICredentials(
        accessToken,
        refreshToken
    );
  }
}


class APIConfig {
  final String apiUrl;
  final OAuthConfig oAuthConfig;

  const APIConfig(this.apiUrl, this.oAuthConfig);

  Uri endpoint(List<String> relativePath, {Map<String, String> query = const {}}) {
    final result = Uri.parse(apiUrl);
    return result.replace(
      pathSegments: [...result.pathSegments, ...relativePath],
      queryParameters: {...result.queryParameters, ...query}
    );
  }

  static const APIConfig devConfig = APIConfig("https://at9jrcjh13.execute-api.us-east-1.amazonaws.com/dev", OAuthConfig.devConfig);
}

class ProfileAvatar {
  final String? avatarUrl;

  const ProfileAvatar(this.avatarUrl);
}

class Profile {
  final String id;
  final String nickname;
  final String name;
  final ProfileAvatar avatar;

  const Profile(this.id, this.nickname, this.name, this.avatar);
}

class ProfilePostPhoto {
  final String id;
  final String url;

  const ProfilePostPhoto(this.id, this.url);
}

class ProfilePost {
  final String postIdentifier;
  final String postDescription;
  final List<ProfilePostPhoto> photos;

  const ProfilePost(this.postIdentifier, this.postDescription, this.photos);
}

class ProfilePosts {
  final List<ProfilePost> posts;

  const ProfilePosts(this.posts);
}

class RegramAPI {
  final APIConfig apiConfig;
  APICredentials? credentials;

  RegramAPI(this.apiConfig);

  Future<http.Response> _makeRequest(String method, Uri uri, {String? body}) async {
    final request = http.Request(
      method, uri,
    );
    if (body != null) {
      request.body = body;
    }
    request.headers.addAll({
      "Authorization": credentials!.accessToken
    });
    final response = await request.send();
    return http.Response.fromStream(response);
  }

  Future<APICredentials> getCredentials(OAuthResponse oAuthResponse) async {
    final response = await http.get(apiConfig.endpoint(["token"], query: {"code": oAuthResponse.code, "redirect_uri": apiConfig.oAuthConfig.callbackUrl}));

    final content = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
    /*
    id_token
    access_token
    refresh_token
    expires_in
     */
    return APICredentials(content["id_token"], content["refresh_token"]);
  }

  Future<Profile> getMe() async {
    final response = await _makeRequest(
        "GET",
        apiConfig.endpoint(
            ["profile", "me"],
        ),
    );
    final content = jsonDecode(utf8.decode(response.bodyBytes)) as Map;

    return Profile(content["id"], content['slug'], content['name'], ProfileAvatar(content["avatarUrl"]));
  }

  Future<void> uploadAvatar(Uint8List bytes) async {
    final response = await _makeRequest(
      "POST",
      apiConfig.endpoint(["profile", "me", "avatar"])
    );
    final uploadUrl = Uri.parse((jsonDecode(utf8.decode(response.bodyBytes)) as Map)["uploadUrl"]);

    final uploadResponse = await http.put(
      uploadUrl,
      body: bytes
    );

    if (uploadResponse.statusCode != 200) {
      throw Exception("Unable to upload avatar");
    }
  }

  Future<ProfilePostPhoto> uploadPostPhoto(Uint8List bytes) async {
    final response = await _makeRequest(
        "POST",
        apiConfig.endpoint(["posts", "photo"])
    );
    final data = jsonDecode(utf8.decode(response.bodyBytes)) as Map;

    final uploadUrl = Uri.parse(data["uploadUrl"]);

    final uploadResponse = await http.put(
        uploadUrl,
        body: bytes
    );

    if (uploadResponse.statusCode != 200) {
      throw Exception("Unable to upload avatar");
    }

    return ProfilePostPhoto(data["identifier"], data["photoUrl"]);
  }

  Future<String> createPost(String profileId, String description, List<String> photoIds) async {
    final response = await _makeRequest(
        "POST",
        apiConfig.endpoint(["profile", profileId, "posts"]),
        body: jsonEncode({"description": description, "photoIds": photoIds})
    );

    final data = jsonDecode(utf8.decode(response.bodyBytes)) as Map;

    return data["id"];
  }

  Future<ProfilePosts> getProfilePosts(String profileId) async {
    final response = await _makeRequest(
      "GET",
      apiConfig.endpoint(
        ["profile", profileId, "posts"]
      )
    );

    final content = jsonDecode(utf8.decode(response.bodyBytes)) as Map;

    final result = ProfilePosts(
      List<ProfilePost>.from(content["posts"].map(
              (postObject) => ProfilePost(postObject["id"], postObject["description"], List<ProfilePostPhoto>.from(
                postObject["photos"].map((photoObject) => ProfilePostPhoto(photoObject["id"], photoObject["url"]))
              ))
      ).toList())
    );

    return result;
  }
}