import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rebraingram/api.dart';


class _ProfilePostWidget extends StatelessWidget {
  final ProfilePost post;

  const _ProfilePostWidget(this.post);

  @override
  Widget build(BuildContext context) {
    final preview = Image.network(post.photos.first.url);

    if (post.photos.length > 1) {
      return Stack(
        children: [
          preview,
          Icon(Icons.add_to_photos_outlined, color: Colors.white)
        ],
      );
    }
    return preview;
  }


}

class _ProfilePostsGridView extends StatelessWidget {
  final ProfilePosts posts;

  const _ProfilePostsGridView(this.posts);

  @override
  Widget build(BuildContext context) {
    return GridView.count(crossAxisCount: 3, children: posts.posts.map((post) => _ProfilePostWidget(post)).toList());
  }
}


class _ProfilePostsState extends State<ProfilePostsWidget> {
  late Future<ProfilePosts> _posts;

  @override
  void initState() {
    super.initState();

    _posts = widget.initialPosts;
    print('init');
  }

  @override
  Widget build(BuildContext context) {
    print('build');
    print(_posts);
    return FutureBuilder<ProfilePosts>(
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const CircularProgressIndicator(value: null);
        }

        print(snapshot.data);
        print(snapshot.error);

        print(snapshot.data!.posts);

        if (snapshot.data!.posts.isEmpty) {
          return const Text("There are no posts so far");
        }

        return _ProfilePostsGridView(snapshot.data!);
      },
      future: _posts,
    );
  }
}

class ProfilePostsWidget extends StatefulWidget {
  final Future<ProfilePosts> initialPosts;

  const ProfilePostsWidget(this.initialPosts);

  @override
  State<StatefulWidget> createState() => _ProfilePostsState();
}