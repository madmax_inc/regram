import 'dart:async';
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:rebraingram/api.dart';

import 'package:http/http.dart' as http;
import 'package:rebraingram/routing/routes.dart';
import 'package:rebraingram/state/common.dart';


class _PostPhotoPreview extends StatelessWidget {
  final Future<ProfilePostPhoto> photoMeta;

  const _PostPhotoPreview(this.photoMeta);

  Future<String> _waitForImage() async {
    final profilePostPhoto = await photoMeta;

    while (true) {
      final response = await http.get(Uri.parse(profilePostPhoto.url));

      if (response.statusCode == 404) {
        await Future.delayed(const Duration(seconds: 1));
        continue;
      }

      if (response.statusCode != 200) {
        throw Exception();
      }

      return profilePostPhoto.url;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const CircularProgressIndicator(value: null);
        }

        return Image.network(snapshot.requireData);
      },
      future: _waitForImage()
    );
  }

}

class _PostPhotosModel {
  final List<Future<ProfilePostPhoto>> photos = [];

  void merge(List<Future<ProfilePostPhoto>> rhs) {
    photos.addAll(rhs);
  }

  Future<List<ProfilePostPhoto>> getAll() {
    return Future.wait(photos);
  }

  Future<List<String>> getPhotosIdentifiers() {
    return getAll().then(
            (photos) => photos.map((photoObj) => photoObj.id).toList()
    );
  }
}

class _PostPhotosState extends State<_PostPhotosWidget> {
  Future<void> _addPhoto() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['jpg'],
        allowMultiple: true
    );

    if (result == null || result.files.isEmpty) {
      return;
    }

    setState(() {
      widget.model.merge(result.files.map((fp) => widget.api.uploadPostPhoto(fp.bytes!)).toList());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: GridView.count(
        scrollDirection: Axis.horizontal,
        crossAxisCount: 1,
        children: [
          ...widget.model.photos.map((photo) => _PostPhotoPreview(photo)),
          IconButton(onPressed: _addPhoto, icon: Icon(Icons.add_a_photo_outlined)),
        ],
      ),
    );
  }
}

class _PostPhotosWidget extends StatefulWidget {
  final RegramAPI api;
  final _PostPhotosModel model;

  _PostPhotosWidget(this.api, this.model);

  @override
  State<StatefulWidget> createState() => _PostPhotosState();
}


class NewPostWidget extends StatelessWidget {
  final RegramAPI api;
  final void Function(AppStateEvent) _navigationCallback;

  const NewPostWidget(this.api, this._navigationCallback);

  Future<void> _publishPost(String description, Future<List<String>> photosIds) async {
    await api.createPost("me", description, await photosIds);
    _navigationCallback(NavigateEvent(HomeRoute()));
  }

  @override
  Widget build(BuildContext context) {
    final photosModel = _PostPhotosModel();
    final descriptionController = TextEditingController();
    return Scaffold(
      appBar: AppBar(title: const Text("Regram")),
      body: Column(
        children: [
          _PostPhotosWidget(api, photosModel),
          Expanded(child: TextField(controller: descriptionController)),
          ElevatedButton(
              onPressed: () async {
                await _publishPost(descriptionController.text, photosModel.getPhotosIdentifiers());
              },
              child: Text("Publish!")
          )
        ],
      ),
    );
  }

}