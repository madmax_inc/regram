import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rebraingram/routing/routes.dart';
import 'package:rebraingram/state/callback.dart';
import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/state/controller.dart';
import 'package:rebraingram/state/login.dart';
import 'package:rebraingram/state/view.dart';
import 'package:rebraingram/state/initial.dart';
import 'package:rebraingram/state/home.dart';

import 'package:rebraingram/api.dart';


class RegramRouterDelegate extends RouterDelegate<RegramRoute>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<RegramRoute>
    implements AppStateEventListener
{
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  final RegramController _controller;
  final RegramView _view;

  RegramRouterDelegate(this._controller, this._view);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: _view.build(_controller.state),
      onPopPage: (route, result) {
        if (!route.didPop(route)) {
          return false;
        }

        _controller.eventHappened(BackInvokedEvent());

        notifyListeners();

        return true;
      },
    );
  }

  @override
  RegramRoute get currentConfiguration {
    return _controller.state.asRoute();
  }

  @override
  Future<void> setNewRoutePath(RegramRoute route) async {
    _controller.eventHappened(NavigateEvent(route));
  }

  @override
  void eventHappened(AppStateEvent event) {
    notifyListeners();
  }
}
