import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:rebraingram/api.dart';
import 'package:rebraingram/oauth.dart';


class _LoginState extends State<LoginScreen> {
  bool _isLoggingIn = false;


  void _tryLogin() async  {
    setState(() {
      _isLoggingIn = true;
    });

    final code = await makeOauthGateway(OAuthConfig.devConfig).getCode();
    print("The code is " + code.code);

    final creds = await widget.api.getCredentials(code);

    if (widget.onLoggedIn != null) {
      widget.onLoggedIn!(creds);
    }
  }

  @override
  Widget build(BuildContext context) {
    final body = _isLoggingIn ? CircularProgressIndicator(value: null) : ElevatedButton(onPressed: _tryLogin, child: Text("Login via Cognito"));

    return Center(
      child: body,
    );
  }

}

class LoginScreen extends StatefulWidget {
  final RegramAPI api;
  final void Function(APICredentials)? onLoggedIn;

  LoginScreen(this.api, {this.onLoggedIn});

  @override
  State<LoginScreen> createState() => _LoginState();
}