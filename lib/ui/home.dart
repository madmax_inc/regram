import 'dart:typed_data';

import 'package:flutter/material.dart';

import 'package:file_picker/file_picker.dart';

import 'package:rebraingram/api.dart';
import 'package:rebraingram/routing/routes.dart';
import 'package:rebraingram/state/common.dart';
import 'package:rebraingram/ui/posts.dart';


class _AvatarWidgetState extends State<_AvatarWidget> {
  late Future<ProfileAvatar> _avatarFuture;

  @override
  void initState() {
    super.initState();

    if (widget.initialFuture == null) {
      _avatarFuture = Future<ProfileAvatar>.value(const ProfileAvatar(null));
      return;
    }

    _avatarFuture = widget.initialFuture!;
  }

  Future<ProfileAvatar> _changeAvatar(Uint8List bytes) async {
    await widget.api.uploadAvatar(bytes);
    return (await widget.api.getMe()).avatar;
  }

  Future<void> _beginChangeAvatar() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg'],
      allowMultiple: false
    );

    if (result == null || result.files.isEmpty) {
      return;
    }
    final fileBytes = result.files.first.bytes;

    setState(() {
      _avatarFuture = _changeAvatar(fileBytes!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ProfileAvatar>(
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const CircularProgressIndicator(value: null);
        }

        final avatarIcon = snapshot.data!.avatarUrl == null ? const Icon(Icons.account_box_outlined) : Image.network(snapshot.data!.avatarUrl!);
        final avatarWidget = CircleAvatar(child: avatarIcon);

        if (!widget.allowUpload) {
          return avatarWidget;
        }
        return Column(
          children: [
            avatarWidget,
            ElevatedButton(onPressed: _beginChangeAvatar, child: Text("Change avatar"))
          ],
        );
      },
      future: _avatarFuture
    );
  }

}

class _AvatarWidget extends StatefulWidget {
  final bool allowUpload;
  final RegramAPI api;
  final Future<ProfileAvatar>? initialFuture;

  _AvatarWidget(this.allowUpload, this.api, this.initialFuture);

  @override
  State<StatefulWidget> createState() => _AvatarWidgetState();

}

class _ProfileHeaderPanel extends StatelessWidget {
  final RegramAPI api;
  final Profile profile;

  const _ProfileHeaderPanel(this.api, this.profile);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _AvatarWidget(true, api, Future<ProfileAvatar>.value(profile.avatar)),

        Text(profile.name)
      ],
    );
  }
}

class _ProfileContentsPanel extends StatelessWidget {
  final RegramAPI _api;
  final Future<Profile> _targetProfile;

  const _ProfileContentsPanel(this._api, this._targetProfile);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: ProfilePostsWidget(_targetProfile.then((profile) => _api.getProfilePosts(profile.id)))
    );
  }

}


class _HomeScreenPanel extends StatelessWidget {
  final RegramAPI _api;

  const _HomeScreenPanel(this._api);

  @override
  Widget build(BuildContext context) {
    final profileFuture = _api.getMe();

    final profileHeader = FutureBuilder<Profile>(
      future: profileFuture,
      builder: (context, snapshot) {
        if (!snapshot.hasData && !snapshot.hasError) {
          return const Center(child: CircularProgressIndicator(value: null));
        }
        return Column(
          children: [
            _ProfileHeaderPanel(_api, snapshot.requireData),
          ],
        );
      },
    );

    return Column(
      children: [
        profileHeader,
        Expanded(child: _ProfileContentsPanel(_api, profileFuture))
      ],
    );
  }
}


class _SearchBarState extends State<_SearchBar> {
  bool _isOpened = false;

  void _toggle() {
    setState(() {
      _isOpened = !_isOpened;
    });
  }

  @override
  Widget build(BuildContext context) {
    final toggleIcon = _isOpened ? Icons.close : Icons.search;
    final toggleButton = IconButton(onPressed: _toggle, icon: Icon(toggleIcon));

    if (!_isOpened) {
      return toggleButton;
    }
    return Row(
      children: [
        const TextField(decoration: InputDecoration(hintText: "search people, posts, messages...")),
        toggleButton,
      ],
    );
  }

}

class _SearchBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchBarState();
}

class HomeScreenSubState {
  final Widget contentWidget;
  final Map<BottomNavigationBarItem, AppStateEvent> navigation;
  final int activePage;
  final MapEntry<IconData, AppStateEvent>? floatingActionButton;

  const HomeScreenSubState(this.contentWidget, this.navigation, this.activePage, this.floatingActionButton);
}

class MyProfileSubState extends HomeScreenSubState {
  MyProfileSubState(RegramAPI api) : super(
    _HomeScreenPanel(api),
    {
      BottomNavigationBarItem(icon: Icon(Icons.account_box), label: "Profile"): NavigateEvent(HomeRoute()),
      BottomNavigationBarItem(icon: Icon(Icons.feed), label: "Feed"): NavigateEvent(FeedRoute())
    },
    0,
    MapEntry(Icons.add, NavigateEvent(NewPostRoute()))
  );
}

class FeedSubState extends HomeScreenSubState {
  FeedSubState(RegramAPI api) : super(
      _HomeScreenPanel(api),
      {
        BottomNavigationBarItem(icon: Icon(Icons.account_box), label: "Profile"): NavigateEvent(HomeRoute()),
        BottomNavigationBarItem(icon: Icon(Icons.feed), label: "Feed"): NavigateEvent(FeedRoute())
      },
      1,
      null,
  );
}


class HomeScreen extends StatelessWidget {
  final void Function(AppStateEvent) _navigationCallback;
  final HomeScreenSubState _subState;

  const HomeScreen(this._navigationCallback, this._subState);

  @override
  Widget build(BuildContext context) {
    final fab = (
        _subState.floatingActionButton != null ?
        FloatingActionButton(child: Icon(_subState.floatingActionButton!.key), onPressed: () {
          _navigationCallback(_subState.floatingActionButton!.value);
        },) :
        null
    );
    return Scaffold(
      appBar: AppBar(title: const Text("Regram"), actions: [_SearchBar()]),
      body: _subState.contentWidget,
      bottomNavigationBar: BottomNavigationBar(
          items: _subState.navigation.keys.toList(),
          currentIndex: _subState.activePage,
          onTap: (newIndex) {
            _navigationCallback(_subState.navigation.values.toList()[newIndex]);
          },
      ),
      floatingActionButton: fab,
    );
  }

}