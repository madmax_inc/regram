import 'package:flutter/widgets.dart' show RouteInformationParser, RouteInformation;

abstract class RegramRoute {
  const RegramRoute();
}

class HomeRoute extends RegramRoute {}
class ProfileRoute extends RegramRoute {
  final String profileId;

  const ProfileRoute(this.profileId);
}
class CallbackRoute extends RegramRoute {
  final String code;

  CallbackRoute(this.code);
}
class FeedRoute extends RegramRoute {}
class NewPostRoute extends RegramRoute {}
class PostRoute extends RegramRoute {
  final String postId;

  const PostRoute(this.postId);
}


class RegramRouteParser extends RouteInformationParser<RegramRoute> {
  @override
  Future<RegramRoute> parseRouteInformation(RouteInformation routeInformation) async {
    print(routeInformation.state);
    print(routeInformation.location);

    if (routeInformation.location == null) {
      return HomeRoute();
    }

    final uri = Uri.parse(routeInformation.location!);

    if (uri.path == '/callback') {
      return CallbackRoute(uri.queryParameters["code"]!);
    }

    if (uri.path == '/feed') {
      return FeedRoute();
    }

    if (uri.path == '/posts/new') {
      return NewPostRoute();
    }

    return HomeRoute();
  }
}
