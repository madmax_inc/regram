import 'dart:async';
import 'dart:html';
import 'package:flutter/foundation.dart' show kIsWeb;


class OAuthConfig {
  final String oauthEndpointUrl;
  final String clientId;
  final String callbackUrl;

  const OAuthConfig(this.oauthEndpointUrl, this.clientId, this.callbackUrl);

  String makeLoginUrl() {
    var loginUrl = Uri.parse(oauthEndpointUrl);
    loginUrl = loginUrl.replace(
      pathSegments: [...loginUrl.pathSegments, "login"],
      queryParameters: {...loginUrl.queryParameters, "response_type": "code", "client_id": clientId, "redirect_uri": callbackUrl}
    );

    return loginUrl.toString();
  }

  static const OAuthConfig devConfig = OAuthConfig("https://regram-test.auth.us-east-1.amazoncognito.com", "718msbl219p9je6tmkqmm7pn14",
                                                   "https://d2zoiwqvz0evzy.cloudfront.net/callback");
}

class OAuthResponse {
  final String code;

  const OAuthResponse(this.code);
}

abstract class OAuthGateway {
  final OAuthConfig oAuthConfig;

  const OAuthGateway(this.oAuthConfig);

  Future<OAuthResponse> getCode();
}


class WebOAuthGateway extends OAuthGateway {
  const WebOAuthGateway(OAuthConfig oAuthConfig) : super(oAuthConfig);

  @override
  Future<OAuthResponse> getCode() async {
    final loginWindow = window.open(oAuthConfig.makeLoginUrl(), "Regram Login");

    final oAuthCode = await window.onMessage.first;

    loginWindow.close();

    return OAuthResponse(oAuthCode.data);
  }
}


OAuthGateway makeOauthGateway(OAuthConfig config) {
  if (kIsWeb) {
    return WebOAuthGateway(config);
  }

  throw Exception("Unsupported platform!");
}
