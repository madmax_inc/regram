#!/bin/bash
python3.8 -m pip install --platform manylinux2010_x86_64 \
                         --implementation cp \
                         --python 3.9 \
                         --only-binary=:all: \
                         --upgrade \
                         --target authorizer-deps/python -r src/lambda_authorizer/authorizer-requirements.txt
cp src/lambda_authorizer authorizer-deps/python --recursive
cd authorizer-deps && zip -r ../authorizer-deps.zip .