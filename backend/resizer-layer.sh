#!/bin/bash
python3.8 -m pip install --platform manylinux2010_x86_64 \
                         --implementation cp \
                         --python 3.9 \
                         --only-binary=:all: \
                         --upgrade \
                         --target resizer-deps/python Pillow
cd resizer-deps && zip -r ../resizer-deps.zip .