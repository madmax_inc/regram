import datetime
import typing
import uuid


class Profile(typing.NamedTuple):
    identifier: uuid.UUID
    slug: str
    name: str
    birthday: datetime.date
    avatar_url: typing.Optional[str]

    def to_json(self):
        return {
            'id': str(self.identifier),
            'slug': self.slug,
            'name': self.name,
            'birthday': self.birthday.isoformat(),
            'avatarUrl': self.avatar_url,
        }
