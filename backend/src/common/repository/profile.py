import datetime
import itertools
import typing
import uuid

import boto3
import botocore.exceptions

from common.model.profile import Profile


class ProfileRepositorySettings(typing.NamedTuple):
    user_pool_id: str
    s3_avatars_bucket: str
    s3_avatars_prefix: str


class ProfileRepositoryException(BaseException):
    pass


class ProfileNotFound(ProfileRepositoryException):
    pass


class ProfileRepository:
    def __init__(self, cognito_client: boto3.client,
                 s3_client: boto3.client, settings: ProfileRepositorySettings):
        self._cognito = cognito_client
        self._s3 = s3_client
        self._settings = settings

    def _parse_profile(self, attrs):
        profile_id = uuid.UUID(attrs['sub'])
        avatar_filename = f'{profile_id}.jpg'

        s3_params = {
            'Bucket': self._settings.s3_avatars_bucket,
            'Key': '/'.join([self._settings.s3_avatars_prefix, avatar_filename])
        }

        avatar_url = None
        try:
            self._s3.head_object(
                **s3_params,
            )
        except botocore.exceptions.ClientError as ex:
            if ex.response['Error']['Code'] != '404':
                raise
        else:
            avatar_url = self._s3.generate_presigned_url(
                'get_object',
                Params=s3_params,
                ExpiresIn=300
            )

        return Profile(
            identifier=profile_id,
            slug=attrs['custom:nickname'],
            name=attrs['name'],
            birthday=datetime.date.fromisoformat(attrs['birthdate']),
            avatar_url=avatar_url,
        )

    def get_profile_by_slug(self, profile_slug: str):
        for user in itertools.chain.from_iterable(
            page['Users']
            for page in self._cognito.get_paginator('list_users').paginate(
                UserPoolId=self._settings.user_pool_id,
                AttributesToGet=[
                    'sub',
                    'custom:nickname',
                    'name',
                    'birthdate',
                ],
            )
        ):
            attrs = {
                attr['Name']: attr['Value']
                for attr in user['Attributes']
            }

            slug = attrs['custom:nickname']
            if slug != profile_slug:
                continue

            return self._parse_profile(attrs)
        raise ProfileNotFound

    def get_profile(self, profile_id: uuid.UUID):
        try:
            user = self._cognito.list_users(
                UserPoolId=self._settings.user_pool_id,
                AttributesToGet=[
                    'sub',
                    'custom:nickname',
                    'name',
                    'birthdate',
                ],
                Limit=6,
                Filter=f'sub = "{profile_id}"'
            )['Users'][0]
        except IndexError:
            raise ProfileNotFound

        attrs = {
            attr['Name']: attr['Value']
            for attr in user['Attributes']
        }

        return self._parse_profile(attrs)
