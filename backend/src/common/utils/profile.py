import uuid

from common.repository.profile import ProfileRepository


def get_profile(event, requesting_user_id, profile_repo: ProfileRepository):
    params = event['pathParameters']

    profile_id = params['profile']

    if profile_id == 'me':
        profile_id = requesting_user_id

    try:
        return profile_repo.get_profile(uuid.UUID(profile_id))
    except ValueError:
        return profile_repo.get_profile_by_slug(profile_id)


def get_profile_id(event, requesting_user_id, profile_repo: ProfileRepository):
    params = event['pathParameters']

    profile_id = params['profile']

    if profile_id == 'me':
        profile_id = requesting_user_id

    try:
        return uuid.UUID(profile_id)
    except ValueError:
        return get_profile(event, requesting_user_id, profile_repo).identifier
