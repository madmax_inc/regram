import itertools
import json
import logging
import os
import tempfile

import boto3
from PIL import Image


_logger = logging.getLogger()

s3 = boto3.client('s3')

S3_POSTS_BUCKET = os.environ['S3_POSTS_BUCKET']
S3_POSTS_UPLOAD_PREFIX = os.environ.get('S3_POSTS_UPLOAD_PREFIX', 'posts/upload')
S3_POSTS_PROCESSED_PREFIX = os.environ.get('S3_POSTS_PREFIX', 'posts')

TARGET_WIDTH = 2048
TARGET_HEIGHT = 1536
TARGET_SIZE = TARGET_WIDTH * TARGET_HEIGHT


def _parse_s3_event(event):
    if event['Event'] == 's3:TestEvent':
        return []
    return [
        key
        for key in [
            record['s3']['object']['key']
            for record in event['Records']
        ]
        if key.startswith(S3_POSTS_UPLOAD_PREFIX)
    ]


def _parse_sqs_event(event):
    return list(
        itertools.chain.from_iterable(
            [
                _parse_s3_event(json.loads(record['body']))
                for record in event['Records']
            ]
        )
    )


def resizer(event, context):
    keys = _parse_sqs_event(event)

    for key in keys:
        filename = key.split('/')[-1]

        result = '/'.join([S3_POSTS_PROCESSED_PREFIX, filename])

        _logger.info('Processing %s, to %s', key, result)

        with tempfile.NamedTemporaryFile(
                'wb+'
        ) as input_fp:
            s3.download_fileobj(S3_POSTS_BUCKET, key, input_fp)

            image = Image.open(input_fp.name)

            size = image.size[0] * image.size[1]

            if size < TARGET_SIZE:
                input_fp.seek(0)
                s3.upload_fileobj(input_fp, S3_POSTS_BUCKET, result)
                continue

            ratio = image.size[0] / image.size[1]

            if ratio > 1.0:
                scale_factor = image.size[0] / TARGET_WIDTH
            else:
                scale_factor = image.size[1] / TARGET_HEIGHT

            new_size = (int(image.size[0] / scale_factor), int(image.size[1] / scale_factor))

            with tempfile.NamedTemporaryFile(
                'wb+'
            ) as output_fp:
                new_image = image.resize(new_size)
                new_image.save(output_fp.name)
                s3.upload_fileobj(output_fp, S3_POSTS_BUCKET, result)
