import base64
import json
import os
import typing

import cryptography.x509

import jwt
import jwt.algorithms

import urllib3


class CognitoSettings(typing.NamedTuple):
    region: str
    user_pool_id: str
    client_id: str

    @classmethod
    def from_env(cls):
        return cls(
            region=os.environ['COGNITO_REGION'],
            user_pool_id=os.environ['USER_POOL_ID'],
            client_id=os.environ['CLIENT_ID'],
        )

    @property
    def well_known_url(self):
        return f'https://cognito-idp.{self.region}.amazonaws.com/{self.user_pool_id}/.well-known/jwks.json'


class JWTChecker:
    def __init__(self, settings: CognitoSettings, pool: urllib3.PoolManager):
        self._settings = settings
        self._pool = pool
        self._keys = dict()

    def update_keys(self):
        response = self._pool.request(method='GET', url=self._settings.well_known_url)
        if response.status != 200:
            raise Exception('Unable to load JWKs')

        response = json.loads(response.data)

        self._keys = {
            jwk['kid']: (_load_public_key(jwk), jwk.get('issuer'))
            for jwk in response['keys']
        }

    def check_jwt(self, token: str):
        assert self._keys, 'Keys should be pre-fetched'
        kid = jwt.get_unverified_header(token)['kid']
        try:
            key, issuer = self._keys[kid]
        except KeyError:
            raise Exception('Unknown key')

        try:
            return jwt.decode(token, key=key, algorithms=['RS256'],
                              audience=self._settings.client_id,
                              issuer=issuer)
        except jwt.InvalidTokenError as ex:
            raise Exception from ex


def _load_public_key(jwk_obj):
    try:
        cert = base64.b64decode(jwk_obj['x5c'][0])
        return cryptography.x509.load_der_x509_certificate(
            cert,
        ).public_key()
    except KeyError:
        return jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(jwk_obj))


checker = JWTChecker(
    settings=CognitoSettings.from_env(),
    pool=urllib3.PoolManager(),
)
checker.update_keys()


def custom_authorizer(f):
    def impl(event, context):
        jwt = event['headers']['authorization']

        try:
            claims = checker.check_jwt(jwt)
        except:
            return _generate_policy('Deny', event['methodArn'], jwt)

        try:
            if f(event, context, claims):
                return _generate_policy('Allow', event['methodArn'], claims['sub'])
            raise Exception
        except:
            return _generate_policy('Deny', event['methodArn'], claims['sub'])

    return impl


def _generate_policy(effect, resource, principal_id):
    return {
        'principalId': principal_id,
        'policyDocument':
            {
                'Version': '2012-10-17',
                'Statement': [
                    {
                        'Action': 'execute-api:Invoke',
                        'Effect': effect,
                        'Resource': resource,
                    }
                ]
            }
    }
