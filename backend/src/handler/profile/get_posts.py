import base64
import itertools
import json
import os
import uuid

import boto3

from common.repository.profile import ProfileNotFound, ProfileRepository, ProfileRepositorySettings
from common.utils.profile import get_profile_id

CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Authorization',
}

cognito = boto3.client('cognito-idp', region_name=os.environ['COGNITO_REGION'])
dynamodb = boto3.client('dynamodb', region_name=os.environ['DYNAMODB_REGION'])
s3 = boto3.client('s3')

DYNAMODB_TABLE = os.environ['DYNAMODB_TABLE']
S3_POSTS_BUCKET = os.environ['S3_POSTS_BUCKET']
S3_POSTS_PROCESSED_PREFIX = os.environ.get('S3_POSTS_PREFIX', 'posts')


profile_repo = ProfileRepository(
    cognito_client=cognito,
    s3_client=s3,
    settings=ProfileRepositorySettings(
        user_pool_id=os.environ['USER_POOL_ID'],
        s3_avatars_bucket=os.environ['S3_AVATARS_BUCKET'],
        s3_avatars_prefix=os.environ.get('S3_AVATARS_PREFIX', 'avatars')
    )
)


def _download_url_for_photo(photo_id):
    s3_download_params = {
        'Bucket': S3_POSTS_BUCKET,
        'Key': '/'.join([S3_POSTS_PROCESSED_PREFIX, f'{photo_id}.jpg'])
    }

    return s3.generate_presigned_url(
        'get_object',
        Params=s3_download_params,
        ExpiresIn=300
    )


def get_posts(event, context):
    if event['httpMethod'] == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': CORS_HEADERS,
        }

    requesting_user_id = event['requestContext']['authorizer']['principalId']

    try:
        profile_id = get_profile_id(event, requesting_user_id, profile_repo)
    except ProfileNotFound:
        return {
            'statusCode': 404,
            'headers': {
                'Content-Type': 'application/json',
                **CORS_HEADERS,
            },
            'body': json.dumps({
                'code': 'USER_NOT_FOUND',
            }),
        }

    posts = [
        {
            'id': item['post_id']['S'],
            'description': item['description']['S'],
            'photos': [
                {'id': photo_id, 'url': _download_url_for_photo(photo_id)}
                for photo_id in json.loads(item['photo_ids']['S'])
            ]
        }
        for item in itertools.chain.from_iterable(
            (
                page['Items']
                for page in dynamodb.get_paginator('query').paginate(
                    TableName=DYNAMODB_TABLE,
                    KeyConditionExpression='author_id = :author',
                    ExpressionAttributeValues={
                        ':author': {
                            'S': str(profile_id),
                        },
                    },
                )
            )
        )
    ]


    return {
        'statusCode': 200,
        'headers': {
            **CORS_HEADERS,
            'Content-Type': 'application/json',
        },
        'body': json.dumps({'posts': posts}),
    }
