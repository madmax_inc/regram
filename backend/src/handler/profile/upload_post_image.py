import json
import os
import uuid

import boto3


CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Authorization',
}


s3 = boto3.client('s3')

S3_POSTS_BUCKET = os.environ['S3_POSTS_BUCKET']
S3_POSTS_UPLOAD_PREFIX = os.environ.get('S3_POSTS_UPLOAD_PREFIX', 'posts/upload')
S3_POSTS_PROCESSED_PREFIX = os.environ.get('S3_POSTS_PREFIX', 'posts')


def get_avatar_upload_url(event, context):
    if event['httpMethod'] == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': CORS_HEADERS,
        }

    new_image_id = str(uuid.uuid4())

    filename = f'{new_image_id}.jpg'

    s3_upload_params = {
        'Bucket': S3_POSTS_BUCKET,
        'Key': '/'.join([S3_POSTS_UPLOAD_PREFIX, filename])
    }
    s3_download_params = {
        'Bucket': S3_POSTS_BUCKET,
        'Key': '/'.join([S3_POSTS_PROCESSED_PREFIX, filename])
    }
    upload_url = s3.generate_presigned_url(
        'put_object',
        Params=s3_upload_params,
        ExpiresIn=300
    )
    download_url = s3.generate_presigned_url(
        'get_object',
        Params=s3_download_params,
        ExpiresIn=300
    )

    return {
        'statusCode': 200,
        'headers': {
            **CORS_HEADERS,
            'Content-Type': 'application/json',
        },
        'body': json.dumps({'identifier': new_image_id, 'uploadUrl': upload_url, 'photoUrl': download_url}),
    }
