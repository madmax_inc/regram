import json
import os

import boto3

from common.repository.profile import ProfileRepository, ProfileRepositorySettings, ProfileNotFound
from common.utils.profile import get_profile


CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Authorization',
}


cognito = boto3.client('cognito-idp', region_name=os.environ['COGNITO_REGION'])
s3 = boto3.client('s3')


profile_repo = ProfileRepository(
    cognito_client=cognito,
    s3_client=s3,
    settings=ProfileRepositorySettings(
        user_pool_id=os.environ['USER_POOL_ID'],
        s3_avatars_bucket=os.environ['S3_AVATARS_BUCKET'],
        s3_avatars_prefix=os.environ.get('S3_AVATARS_PREFIX', 'avatars')
    )
)


def get_profile_handler(event, context):
    print(event)

    if event['httpMethod'] == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': CORS_HEADERS,
        }

    requesting_user_id = event['requestContext']['authorizer']['principalId']

    try:
        profile = get_profile(event, requesting_user_id, profile_repo)
    except ProfileNotFound:
        return {
            'statusCode': 404,
            'headers': {
                'Content-Type': 'application/json',
                **CORS_HEADERS,
            },
            'body': json.dumps({
                'code': 'USER_NOT_FOUND',
            }),
        }

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json',
            **CORS_HEADERS,
        },
        'body': json.dumps(profile.to_json()),
    }
