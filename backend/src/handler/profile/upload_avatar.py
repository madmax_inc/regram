import json
import os

import boto3

from common.repository.profile import ProfileRepositorySettings, ProfileRepository
from common.utils.profile import get_profile_id

CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Authorization',
}


cognito = boto3.client('cognito-idp', region_name=os.environ['COGNITO_REGION'])
s3 = boto3.client('s3')

S3_AVATARS_BUCKET = os.environ['S3_AVATARS_BUCKET']
S3_AVATARS_PREFIX = os.environ.get('S3_AVATARS_PREFIX', 'avatars')

profile_repo = ProfileRepository(
    cognito_client=cognito,
    s3_client=s3,
    settings=ProfileRepositorySettings(
        user_pool_id=os.environ['USER_POOL_ID'],
        s3_avatars_bucket=S3_AVATARS_BUCKET,
        s3_avatars_prefix=S3_AVATARS_PREFIX,
    )
)


def get_avatar_upload_url(event, context):
    if event['httpMethod'] == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': CORS_HEADERS,
        }

    requesting_user_id = event['requestContext']['authorizer']['principalId']
    profile_id = get_profile_id(event, requesting_user_id, profile_repo)

    avatar_filename = f'{profile_id}.jpg'

    s3_params = {
        'Bucket': S3_AVATARS_BUCKET,
        'Key': '/'.join([S3_AVATARS_PREFIX, avatar_filename])
    }
    upload_url = s3.generate_presigned_url(
        'put_object',
        Params=s3_params,
        ExpiresIn=300
    )

    return {
        'statusCode': 200,
        'headers': {
            **CORS_HEADERS,
            'Content-Type': 'application/json',
        },
        'body': json.dumps({'uploadUrl': upload_url}),
    }
