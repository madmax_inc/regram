import base64
import json
import os
import uuid

import boto3

from common.repository.profile import ProfileNotFound, ProfileRepository, ProfileRepositorySettings
from common.utils.profile import get_profile_id

CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Authorization',
}

cognito = boto3.client('cognito-idp', region_name=os.environ['COGNITO_REGION'])
dynamodb = boto3.client('dynamodb', region_name=os.environ['DYNAMODB_REGION'])
s3 = boto3.client('s3')

DYNAMODB_TABLE = os.environ['DYNAMODB_TABLE']
S3_POSTS_BUCKET = os.environ['S3_POSTS_BUCKET']
S3_POSTS_PROCESSED_PREFIX = os.environ.get('S3_POSTS_PREFIX', 'posts')


profile_repo = ProfileRepository(
    cognito_client=cognito,
    s3_client=s3,
    settings=ProfileRepositorySettings(
        user_pool_id=os.environ['USER_POOL_ID'],
        s3_avatars_bucket=os.environ['S3_AVATARS_BUCKET'],
        s3_avatars_prefix=os.environ.get('S3_AVATARS_PREFIX', 'avatars')
    )
)


def create_post(event, context):
    if event['httpMethod'] == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': CORS_HEADERS,
        }

    requesting_user_id = event['requestContext']['authorizer']['principalId']

    try:
        profile_id = get_profile_id(event, requesting_user_id, profile_repo)
    except ProfileNotFound:
        return {
            'statusCode': 404,
            'headers': {
                'Content-Type': 'application/json',
                **CORS_HEADERS,
            },
            'body': json.dumps({
                'code': 'USER_NOT_FOUND',
            }),
        }

    post_id = str(uuid.uuid4())
    body = event['body']
    if event['isBase64Encoded']:
        body = base64.b64decode(body)
    body = json.loads(body)

    dynamodb.put_item(
        TableName=DYNAMODB_TABLE,
        Item={
            'author_id': {'S': str(profile_id)},
            'post_id': {'S': post_id},
            'description': {'S': body['description']},
            'photo_ids': {'S': json.dumps(body['photoIds'])}
        }
    )

    return {
        'statusCode': 200,
        'headers': {
            **CORS_HEADERS,
            'Content-Type': 'application/json',
        },
        'body': json.dumps({'id': post_id}),
    }
