import os

import boto3

from common.repository.profile import ProfileRepository, ProfileRepositorySettings
from common.utils.profile import get_profile_id
from lambda_authorizer.authorizer import custom_authorizer


cognito = boto3.client('cognito-idp', region_name=os.environ['COGNITO_REGION'])
s3 = boto3.client('s3')

profile_repo = ProfileRepository(
    cognito_client=cognito,
    s3_client=s3,
    settings=ProfileRepositorySettings(
        user_pool_id=os.environ['USER_POOL_ID'],
        s3_avatars_bucket=os.environ['S3_AVATARS_BUCKET'],
        s3_avatars_prefix=os.environ.get('S3_AVATARS_PREFIX', 'avatars')
    )
)


@custom_authorizer
def profile_authorizer(event, context, jwt_claims):
    requesting_user_id = jwt_claims['sub']

    profile_id = get_profile_id(event, requesting_user_id, profile_repo)

    if str(profile_id) == requesting_user_id:
        return True

    return event['httpMethod'] == 'GET'
