import os

import boto3
import urllib3

CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
}

TOKEN_URL = os.environ['OAUTH2_TOKEN_URL']
CLIENT_ID = os.environ['CLIENT_ID']

cognito = boto3.client('cognito-idp', region_name='us-east-1')
client_secret = cognito.describe_user_pool_client(
    UserPoolId=os.environ['USER_POOL_ID'],
    ClientId=CLIENT_ID,
)['UserPoolClient']['ClientSecret']

http = urllib3.PoolManager()


def lambda_handler(event, context):
    if event['httpMethod'] == 'OPTIONS':
        return {
            'statusCode': 200,
            'headers': CORS_HEADERS,
        }

    query = event['queryStringParameters'] or {}

    payload = {
        'grant_type': 'authorization_code',
        'client_id': CLIENT_ID,
        'code': query['code'],
        'redirect_uri': query['redirect_uri'],
    }

    response = http.request(
        method='POST',
        url=TOKEN_URL,
        headers={
            **urllib3.make_headers(basic_auth=':'.join([CLIENT_ID, client_secret])),
        },
        fields=payload,
        encode_multipart=False,
    )

    return {
        'statusCode': response.status,
        'headers': {
            **response.headers,
            **CORS_HEADERS,
        },
        'body': response.data,
    }
